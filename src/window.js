<div id="ppitest" style="width:1in;visible:hidden;padding:0px"></div>
<input type=hidden name="wid" id="wid" value="0">
<input type=hidden name="hgt" id="hgt" value="0">
<input type=hidden name="ppi" id="ppi" value="0">

<script type="text/javascript">
  var myWidth = 0, myHeight = 0;
  if( typeof( window.innerWidth ) == 'number' ) {
    //Non-IE
    myWidth = window.innerWidth;
    myHeight = window.innerHeight;
  } else if( document.documentElement && ( document.documentElement.clientWidth || document.documentElement.clientHeight ) ) {
    //IE 6+ in 'standards compliant mode'
    myWidth = document.documentElement.clientWidth;
    myHeight = document.documentElement.clientHeight;
  } else if( document.body && ( document.body.clientWidth || document.body.clientHeight ) ) {
    //IE 4 compatible
    myWidth = document.body.clientWidth;
    myHeight = document.body.clientHeight;
  }

  document.getElementById("wid").value = myWidth;
  document.getElementById("hgt").value = myHeight;
  document.getElementById("ppi").value = document.getElementById("ppitest").offsetWidth;
</script>
