#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <time.h>
#include "key.h"
#include "auto/keystruct.h"
#include "page.h"

int valid_key = 0;
int valid_parms = 1;
char *user_agent = "";
char *client_ip = "";
long window_height = 0;
long window_width = 0;
long window_ppi = 0;

#define ACTION_NEXT 0
#define ACTION_PREV 1
int action = ACTION_NEXT;

void eval_element(char *k,char *v,int only_key) {
	if (only_key) {
		if (strcmp(k,"key") == 0)
		        valid_key = eval_key(v,(unsigned char *)&key,KEY_LEN);
	}
	else if (!only_key) {
		if (strncmp(k,"wid",3) == 0)
			window_width = strtol(v,NULL,10);
		else if (strncmp(k,"hgt",3) == 0)
			window_height = strtol(v,NULL,10);
		else if (strncmp(k,"ppi",3) == 0)
			window_ppi = strtol(v,NULL,10);
		else if (eval_parm(k,v) == 0)
			valid_parms = 0;
	}
}

void eval_query(char *x,int only_key) {
	char buf[10000+3];
	char *k,*s,*v,c;
	int last;
	
	if ((strlen(x) == 0) || (strlen(x) >= 10000))
		return;

	strncpy(&buf[1],x,10000);
	x = &buf[1];
	s = buf;
		
	last = 0;
	while (last == 0) {
		k = s;
		v = NULL;
		while((*x != '&') && (*x != 0)) {
			c = *x;
			if (c == '=') {
				*s++ = 0;
				v = s;
			}
			else {
				if (c == '+')
					c = ' ';
				if (c == '%') {
					if (isxdigit(x[1]) && isxdigit(x[2])) {
						int i;
						c = 0;
						for (i = 0;i <= 1;i++) {
							c <<= 4;
							if ((x[1] >= '0') && (x[1] <= '9'))
								c |= x[1] - '0';
							if ((x[1] >= 'a') && (x[1] <= 'f'))
								c |= x[1] - 'a' + 10;
							if ((x[1] >= 'A') && (x[1] <= 'F'))
								c |= x[1] - 'A' + 10;
							x++;
						}
					}
				}
				*s++ = c;
			}
			x++;
		}
		last = (*x == 0);
		x++;
		*s = 0;
		eval_element(k,v,only_key);
	}
}

void eval_env(char **env) {
	char **x = env;
	int post = 0;
	char *query = NULL;
	char postbuf[1000];
	long CLEN=0;
	
	postbuf[0] = 0;
	while(*x) {
		if (strncmp(*x,"QUERY_STRING=",13) == 0) {
			query = &(*x)[13];
		}
		if (strncmp(*x,"REQUEST_METHOD=POST",19) == 0) {
			post = 1;
		}
		if (strncmp(*x,"CONTENT_LENGTH=",15) == 0) {
			CLEN = strtol(&(*x)[15],NULL,10);
		}
		if (strncmp(*x,"HTTP_USER_AGENT=",16) == 0) {
			user_agent = &(*x)[16];
		}
		if (strncmp(*x,"REMOTE_ADDR=",12) == 0) {
			client_ip = &(*x)[12];
		}
		x++;
	}
	if (post == 1) {
		if ((CLEN > 0) && (CLEN < 1000-1)) {
			fread(postbuf,1,CLEN,stdin);
			query = postbuf;
		}
	}
	if (query) {
		eval_query(query,1);
		empty_checkboxes(key.page);
		eval_query(query,0);
	}
}

void body() {
	puts("<div id=\"doc\">");
	puts("<header id=\"header\">");
	header();
	puts("</header>");

	puts("<div class=\"rain\">");
	puts("<div class=\"border start\">");
	puts("<section id=\"section\">");
	puts("<form id=questions>");
	questions(key.page);

#include "auto/window.h"
#ifdef DEBUG
	if (DEBUG)
		printf("<input type=text name=key value=\"");
	else 
		printf("<input type=hidden name=key value=\"");
#else
	printf("<input type=hidden name=key value=\"");
#endif
	print_encoded_key((unsigned char *)&key,KEY_LEN);
	printf("\">\n");

	puts("</form>");
	puts("</section>");
	puts("</div>");
	puts("</div>");
	
//	puts("<aside>");
//	puts("aside");
//	puts("</aside>");
//	puts("<footer>");
//	puts("footer");
//	puts("</footer>");
	puts("</div>");
}

void write_key() {
	struct timespec t;
	char fname[100];
	FILE *f;
	
	if (clock_gettime(CLOCK_MONOTONIC_RAW,&t) == 0) {
		sprintf(fname,"../log/%ld_%ld",t.tv_sec,t.tv_nsec);
		f = fopen(fname,"w");
		if (f) {
			fwrite(&key,KEY_LEN,1,f);
			fputc('\n',f);
			fwrite(user_agent,strlen(user_agent),1,f);
			fputc('\n',f);
			fwrite(client_ip,strlen(client_ip),1,f);
			fputc('\n',f);
			fclose(f);
		}
		sync();
	}
}

void init_key(void) {
	memset(&key,0,KEY_LEN);
}

int main(int argc,char **argv,char **env) {
	int n;
	
	assert(sizeof(struct key_s) == KEY_LEN);
	
	n = strlen(argv[0]);
	if (strcmp(".prev",&argv[0][n-5]) == 0)
		action = ACTION_PREV;

	memset(&parmerr,0,sizeof(struct parmerr_s));
	eval_env(env);
	if (valid_key != 1)
		init_key();
	if (valid_parms && valid_key) {
		if (action == ACTION_NEXT)
	 	      do { key.page++; } while (!present_page(key.page));
		if (action == ACTION_PREV)
	 	      do { key.page--; } while (!present_page(key.page));
	}
	
	puts("Content-type: text/html\n");
	puts("<!DOCTYPE html>");
	puts("<html lang=\"en\">");
	puts("<head>");
	puts("<meta charset=\"utf-8\">");
	puts("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0\"/>");
	puts("<title>");
	title();
	puts("</title>");	
//	puts("<link rel=\"stylesheet\" href=\"http://www.inserthtml.com/demo/webkit-form/style2.css?v=1.0\" type=text/css>");
	puts("<!--[if lt IE 9]>");
	puts("<script src=\"http://html5shiv.googlecode.com/svn/trunk/html5.js\"></script>");
	puts("<![endif]-->");
#include "auto/script.h"
	puts("<style>");
#include "auto/style.h"
	puts("</style>"); 
	puts("</head>"); 
	puts("<body>");

#ifdef DEBUG
	if (DEBUG) {
		print_encoded_key((unsigned char *)&key,KEY_LEN);
		printf("  valid_key=%d<p>\n",valid_key);
	}
#endif

	body();

#ifdef DEBUG
	if (DEBUG) {
		puts("<h2>Environment</h2>");
		while (*env) {
			printf("%s<p>\n",*env++);
		}
	}
#endif

	puts("</body>");
	puts("</html>");
	
	write_key();
	
	return 0;
}
