#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include "key.h"

// 6 bits are encoded with the characters:
//      0..9 for 0-9
//      a..z for 10-35
//      A..Z for 36-61
//      .    for 62
//      -    for 63

void scramble_pattern(unsigned char *key,int keylen) {
	int i;
	unsigned char m;
	unsigned char p;
	
	m = 0;
	for (i = 0;i < keylen;i++)
		m ^= key[i];
	if (m == 0)
		m = 0x5a;

	p = m;
	m = 0;
	for (i = 0;i < keylen;i++) {
		key[i] ^= p;
		m ^= p;
		if (p & 1)
			p = (p >> 1) ^ 0xfa;
		else
			p >>= 1;
	}
	key[0] ^= m; // ensure pattern xor sum is 0
}

int eval_key(const char *v,unsigned char *key,int keylen) {
	const char *x;
	unsigned char *k = key;
	unsigned char hexbits[256];
	unsigned char hexsum;
	int i;
	int j;
	int mask,n_mask;
	
	assert(keylen*8 < (256-1)*6);
	
	// Check all chars in value being correct
	i = 0;
	hexsum = 0;
	for (x = v;*x != 0;x++) {
		char c = *x;
		char n;
		
		if ((c >= '0') && (c <= '9'))
			n = c-'0';
		else if ((c >= 'a') && (c <= 'z'))
			n = c-'a'+10;
		else if ((c >= 'A') && (c <= 'Z'))
			n = c-'A'+36;
		else if (c == '.')
			n = 62;
		else if (c == '-')
			n = 63;
		else
			return 0;
		hexbits[i++] = n;
		if (i >= 256)
			return 0;
		hexsum += n;
		if (hexsum >= 64)
			hexsum -= 64;
	}
	if (hexsum != 63)
		return 0;
	if (keylen*8 < (i-2)*6)
		return 0;
	if (keylen*8 > (i-1)*6)
		return 0;

	mask = 0;
	n_mask = 0;
	for (j = 0;j < i-1;j++) {
		mask <<= 6;
		mask |= hexbits[j];
		n_mask += 6;
		if (n_mask >= 8) {
			*key++ = mask>>(n_mask-8);
			mask &=  255>>(16-n_mask);
			n_mask -= 8;
		}
	}
	if (n_mask > 0) {
		mask = mask<<(8-n_mask);
		*key = mask;
	}
 	scramble_pattern(k,keylen);
	return 1;
}

char hexencode(unsigned char h){
	if (h < 10)
		return '0'+h;
	if (h < 36)
		return 'a'+(h-10);
	if (h < 62)
		return 'A'+(h-36);
	if (h == 62)
		return '.';
	if (h == 63)
		return '-';
	return '#';
}

void print_encoded_key(unsigned char *key,int keylen) {
	unsigned char hexbits[256],*h;
	unsigned char hexsum,n;
	int i;
	unsigned int mask,n_mask;
	unsigned char *k = key;
	
	assert(keylen*8 < (256-1)*6);

 	scramble_pattern(k,keylen);
	
	mask = 0;
	n_mask = 0;
	hexsum = 0;
	h = hexbits;
	for (i = 0;i < keylen;i++) {
		mask <<= 8;
		mask |= *key++;
		n_mask += 8;
		while (n_mask >= 6) {
			n = mask>>(n_mask-6);
			mask &= (1<<(n_mask-6))-1;
			n_mask -= 6;
			*h++ = hexencode(n);
			hexsum += n;
			if (hexsum >= 64)
				hexsum -= 64;
		}
	}
	if (n_mask > 0) {
		mask = mask<<(8-n_mask);
		n = mask>>2;
		*h++ = hexencode(n);
		hexsum += n;
		if (hexsum >= 64)
			hexsum -= 64;
	}
	*h++ = hexencode(63-hexsum);
	*h = 0;
	
 	scramble_pattern(k,keylen);

	printf("%s",hexbits);
}
