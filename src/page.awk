BEGIN {
	func_aff=""
	elem_aff=""
	page=-1
	elem=0
	required=""
#	required="required"
	print "#include <stdio.h>"
	print "#include <string.h>"
	print "#include <assert.h>"
	print "#include \"../page.h\""
	print "#include \"keystruct.h\""
	radio = " type=radio class=\\\"regular-radio\\\" "
	radio_checked = radio "checked=\\\"checked\\\" "

	checkbox = " type=checkbox class=\\\"regular-checkbox\\\" "
	checkbox_checked = checkbox "checked=\\\"checked\\\" "
	errstyle="\" style=\\\"background-color:red;\\\" \""

	butNext="Next"
	butPrev="Previous"
}

function map(n) {
	if (n < 26)
		return sprintf("%c",n+65)
	if (n < 52)
		return sprintf("%c",n+97-26)
	return sprintf("%c",n+48-52)
}

function next_elem(range    ,s,r,bits,m) {
	r = int(elem/52)
	s = map(elem-r*52) map(r)
	el_range[s] = range
	m=2
	for (bits=1;m < range;bits++)
		m *= 2
	el_bits[s] = bits
	elem++
	return s
}

/^#/ {next}

{
	gsub(/&/,"\\&amp;")
	gsub(/>/,"\\&gt;")
	gsub(/</,"\\&lt;")
	gsub(/"/,"\\&quot;")
	gsub(/&amp;&amp;/,"\\&\\&")
}

/^LANG:/ {
	gsub(/^LANG: */,"")
	lang = $0
}
/^[a-zA-Z][a-zA-Z]\-/ {
	if (substr($0,1,2) != lang)
		next
	gsub(/^[a-zA-Z][a-zA-Z]\-/,"")
}


/^NEXT:/ {
	gsub(/^NEXT: */,"")
	butNext = $0
}
/^PREV:/ {
	gsub(/^PREV: */,"")
	butPrev = $0
}

/^Title:/ {
	gsub(/^Title: */,"")
	print "void title(void) {"
	print "   printf(\"%s\\n\",\"" $0 "\");"
	print "}"
}


/^Header:/ {
	gsub(/^Header: */,"")
	print "void header(void) {"
	print "   printf(\"%s\\n\",\"" $0 "\");"
	print "}"
}


/^Page:/ {
	page++
	if (elem_aff != "") {
		print elem_aff
		elem_aff = ""
	}
	if (func_aff != "") {
		print func_aff
		func_aff = ""
	}
	print "static void page_"page"(int next,int prev) {"
	func_aff = func_aff "   puts(\"<fieldset><div class=\\\"button-holder\\\">\");\n"
	#usepost = "formmethod=Post"
	func_aff = func_aff "   if (prev) puts(\"<input type=\\\"submit\\\" " usepost " formaction=\\\"survey.prev\\\" onclick='this.form.action=\\\"survey.prev\\\";' value=\\\"" butPrev "\\\">\");\n"
	func_aff = func_aff "   if (next) puts(\"<input type=\\\"submit\\\" formaction=\\\"survey\\\"  onclick='this.form.action=\\\"survey\\\";' value=\\\"" butNext "\\\">\");\n"
	func_aff = func_aff "   puts(\"</div></fieldset>\");\n"
	func_aff = func_aff "}\n"
	af = " autofocus=\\\"autofocus\\\" "

	if ($2 ~ /^\(.*\)$/) 
		pagecond[page] = $2
}

/^Section:/ {
}

/^Q:/{
	if (elem_aff != "") {
		print elem_aff
		elem_aff = ""
	}
	if ($2 ~ /^\(.*\)$/) {
		elem_aff = "   }"
		print "   if " $2 " {"
		$2 = ""
	}

	gsub(/^Q: */,"")
	q = $0
	for (i in flagtag)
		delete flagtag[i]
}

/^TQ:/ {
	tq_aff = ""
	if ($2 ~ /^\(.*\)$/) {
		tq_aff = "   }"
		print "   if " $2 " {"
		$2 = ""
	}
	gsub(/^TQ: */,"")
	print "   puts(\"<tr><td>" $0 "</td>\");"

	# 0 is for not selected, 1..n for the selection
	e = next_elem(n+1)
	eval[e] = "0-n"
	for (i = 1;i <= tn;i++) {
		if (tr[i] in flagtag)
			flagcond[flagtag[tr[i]]] = "(key."e" == " i ")"
		common="<td class=snug><input value=\\\""i"\\\" id=\\\"" e i"\\\" name=\\\""e"\\\"" af required
		print "   if (key."e" == "i")"
		print "      puts(\"" common radio_checked ">\");"
		print "   else"
		print "      puts(\"" common radio ">\");"
		print "   puts(\"<label for=" e i ">" tr[i] "</label></td>\");"
		af = ""
	}
	print "   puts(\"</tr>\");"
	for (i in flagtag)
		delete flagtag[i]
	if (tq_aff != "") {
		print tq_aff
		tq_aff = ""
	}
}
/^TR:/ {
	gsub(/^TR: */,"")
	tn = split($0,tr,"/")
	print "   puts(\"<fieldset>\");"
	print "   puts(\"<legend>" q "</legend>\");"
	print "   puts(\"<table border=\\\"0\\\">\");"
	elem_aff = "   puts(\"</table></fieldset>\");\n" elem_aff
}

/^F:/ {
	gsub(/^F: */,"")
	split($0,r,"=")
	flagtag[r[2]] = r[1]
}

/^R:/ {
	gsub(/^R: */,"")
	n = split($0,r,"/")
	print "   puts(\"<fieldset>\");"
	print "   puts(\"<legend>" q "</legend>\");"
	# 0 is for not selected, 1..n for the selection
	e = next_elem(n+1)
	print "   puts(\"<div class=\\\"button-holder\\\">\");"
	eval[e] = "0-n"
	for (i = 1;i <= n;i++) {
		if (r[i] in flagtag) 
			flagcond[flagtag[r[i]]] = "(key."e" == " i ")"
		common="<input value=\\\""i"\\\" id=\\\"" e i"\\\" name=\\\""e"\\\"" af required
		print "   if (key."e" == "i")"
		print "      puts(\"" common radio_checked ">\");"
		print "   else"
		print "      puts(\"" common radio ">\");"
		print "   puts(\"<label for=" e i ">" r[i] "</label>\");"
		af = ""
	}
	print "   puts(\"</div></fieldset>\");"
}

/^D:/ {
	gsub(/^D: */,"")
	n = split($0,r,"/")
	print "   puts(\"<fieldset>\");"
	print "   puts(\"<legend>" q "</legend>\");"
	# 0 is for not selected, 1..n for the selection
	e = next_elem(n+1)
	eval[e] = "0-n"
	print "   puts(\"<select name=\\\""e"\\\" "af" "required">\");"
	af=""
	print "   puts(\"<option value=\\\"0\\\">----</option>\");"
	for (i = 1;i <= n;i++) {
		if (r[i] in flagtag)
			flagcond[flagtag[r[i]]] = "(key."e" == " i ")"
		print "   if (key."e" == "i")"
		print "      puts(\"<option value=\\\""i"\\\" selected>"r[i]"</option>\");"
		print "   else"
		print "      puts(\"<option value=\\\""i"\\\">"r[i]"</option>\");"
	}
	print "   puts(\"</select>\");"
	print "   puts(\"</fieldset>\");"
}

/^C:/ {
	gsub(/^C: */,"")
	n = split($0,r,"/")
	print "   puts(\"<fieldset>\");"
	print "   puts(\"<legend>" q "</legend>\");"
	for (i = 1;i <= n;i++) {
		e = next_elem(2)
		if (r[i] in flagtag)
			flagcond[flagtag[r[i]]] = "(key."e" == 1)"
		checkboxes[page] = checkboxes[page] ":" e
		eval[e] = "0-n"
		common = "<input value=\\\""1"\\\" " af " id=\\\"" e "\\\" name=\\\""e"\\\" "
		print "   if (key."e" == 1)"
		print "      puts(\"" common checkbox_checked ">\");"
		print "   else"
		print "      puts(\"" common checkbox ">\");"
		print "   puts(\"<label for=" e ">"r[i]"</label>\");"
		af = ""
	}
	print "   puts(\"</fieldset>\");"
}

/^A:/ {
	gsub(/^A: */,"")
	print "   puts(\"<fieldset>\");"
	print "   puts(\"<legend>" q "</legend>\");"
	if ($0 == "Date") {
		e = next_elem(2013-15-1970+1+1)
		eval[e] = "0-n"
		print "   printf(\"<select name=\\\""e"\\\" "af" %s "required">\\n\","
		print "       parmerr."e"?"errstyle":\"\");"
		for(y = 1969;y <= 2013-15;y++) { # 1969 maps to ----
			lab = y
			if (y == 1969)
				lab = "----"
			print "   if ("y-1969" == key."e")"
			print "       puts(\"<option value=\\\""y-1969"\\\" selected>"lab"</option>\");"
			print "   else"
			print "       puts(\"<option value=\\\""y-1969"\\\">"lab"</option>\");"
		}
		print "   puts(\"</select>\");"
		e = next_elem(13)
		eval[e] = "0-n"
		print "   printf(\"<select name=\\\""e"\\\" "af" %s "required">\\n\","
		print "       parmerr."e"?"errstyle":\"\");"
		print "   puts(\"<option value=\\\"X\\\">--</option>\");"
		for(m = 0;m <= 12;m++) {
			lab = m
			if (m == 0)
				lab = "--"
			print "   if ("m" == key."e")"
			print "       puts(\"<option value=\\\""m"\\\" selected>"lab"</option>\");"
			print "   else"
			print "       puts(\"<option value=\\\""m"\\\">"lab"</option>\");"
		}
		print "  puts(\"</select>\");"
		e = next_elem(32)
		eval[e] = "0-n"
		print "   printf(\"<select name=\\\""e"\\\" "af" %s "required">\\n\","
		print "       parmerr."e"?"errstyle":\"\");"
		for(d = 0;d <= 31;d++) {
			lab = d
			if (d == 0)
				lab = "--"
			print "   if ("d" == key."e")"
			print "      puts(\"<option value=\\\""d"\\\" selected>"lab"</option>\");"
			print "   else"
			print "      puts(\"<option value=\\\""d"\\\">"lab"</option>\");"
		}
		print "   puts(\"</select>\");"
	}
	else if (/Number [0-9]*-[1-9][0-9]*/) {
		e = next_elem(256)
		eval[e] = "#"
		elen[e] = $2
		print "   printf(\"<input type="$0" name=\\\""e"\\\" "af" %s value=\\\"%s\\\" "required">\","
		print "       parmerr."e"?"errstyle":\"\",as_text_"e"());"
	}
	else if (/Text [0-9]*-[1-9][0-9]*/) {
		e = next_elem(256)
		eval[e] = "@"
		elen[e] = $2
		print "   printf(\"<input type="$0" name=\\\""e"\\\" "af" %s value=\\\"%s\\\" "required">\","
		print "       parmerr."e"?"errstyle":\"\",as_text_"e"());"
	}
	else
		print "Unknown TAG: "$0 >"/dev/stderr"
	af=""
	print "   puts(\"</fieldset>\");"
}

function eval_number(e    ,fld,n,var) {
	split(elen[e],fld,"-")
	n = fld[2]
	while (n >= 3) {
		var = "key." i "3_" int(n/3)
		print "   "var" = 0;"
		print "   if (*v) {"
		print "      int n = strlen(v);"
		print "      if (n >= 3) {"
		print "         "var" = 1+(v[0]-'0')*100+(v[1]-'0')*10+(v[2]-'0');"
		print "         v += 3;"
		print "      }"
		print "      else if (n >= 1)"
		print "         "var" = 1001+ (*v++)-'0';"
		print "   }"
		n-=3
	}
	if ((n == 1) || (fld[2] == 3)) {
		var = "key." i "1"
		print "   "var" = 0;"
		print "   if (*v) {"
		print "      if (strlen(v) == 1)"
		print "         "var" = 1+(*v++)-'0';"
		print "   }"
	}
	if (n == 2) {
		var = "key." i "2"
		print "   "var" = 0;"
		print "   if (*v) {"
		print "      int n = strlen(v);"
		print "      if (n == 2) {"
		print "         "var" = 1+(v[0]-'0')*10+(v[1]-'0');"
		print "         v += 2;"
		print "      }"
		print "      else if (n == 1)"
		print "         "var" = 101+ (*v++)-'0';"
		print "   }"
	}
	print "   return (*v == 0);"
}

function eval_text(e   ,fld) {
	split(elen[e],fld,"-")
	print "   for (i = 0;i < " fld[2]";i++) {"
	print "      key."e"[i] = *v;"
	print "      if (*v)"
	print "         v++;"
	print "   }"
	print "   return 1;"
}

function in_buf_number(e  ,fld) {
	split(elen[e],fld,"-")
	n = fld[2]
	while (n >= 3) {
		var = "key." i "3_" int(n/3)
		print "   if ("var" != 0) {"
		print "      if ("var" <= 1000) {"
		print "         char tmp[5];"
		print "         sprintf(tmp,\"%d\","var"+1000-1);"
		print "         strncpy(buf,tmp+1,3);"
		print "         buf += 3;"
		print "      }"
		print "      else"
		print "         *buf++ = '0' + ("var"-1001);"
		print "   }"
		n-=3
	}
	if ((n == 1) || (fld[2] == 3)) {
		var = "key." i "1"
		print "   if (("var" != 0) && ("var" <= 10))"
		print "         *buf++ = '0' + "var"-1;"
	}
	if (n == 2) {
		print "   if ("var" != 0) {"
		print "      if ("var" <= 100) {"
		print "         char tmp[4];"
		print "         sprintf(tmp,\"%d\","var"+100-1);"
		print "         strncpy(buf,tmp+1,2);"
		print "         buf += 2;"
		print "      }"
		print "      else"
		print "         *buf++ = '0' + ("var"-101);"
		print "   }"
	}
	print "   *buf = 0;"
}
function in_buf_text(e  ,fld) {
	split(elen[e],fld,"-")
	print "   strncpy(buf,key."e","fld[2]");"
	print "   buf["fld[2]"] = 0;"
}

END {
	if (elem_aff != "") {
		print elem_aff
		elem_aff = ""
	}
	if (func_aff != "") {
		print func_aff
		func_aff = ""
	}
	print "void questions(unsigned int page) {"
	for (i = 0;i <= page;i++) {
		print "  if (page == " i ") page_" i "("(i!=page)","(i != 0)");"
	}
	print "}"

	print "static int get_val(const char *v) {"
	print "   int p=-1;"
	print "   while (*v) {"
	print "       if ((*v >= '0') && (*v <= '9')) {"
	print "		 if (p < 0) p = 0;"
	print "		 p = p*10 + (*v++) - '0';"
	print "       }"
	print "       else"
	print "          return -1;"
	print "   }"
	print "   return p;"
	print "}"

	for (i in el_range) {
		print "static int eval_"i"(const char *v) {"

		if (eval[i] == "0-n") {
			print "   int p = get_val(v);"
			print "   if (p < 0) { parmerr."i" = 1; return 0; }"
			print "   if (p >= "el_range[i]") { parmerr."i" = 1; return 0; }"
			print "   key."i" = p;"
			print "   return 1;"
		}
		else if (eval[i] ~ /[#@]/) {
			print "   int i;"
			split(elen[i],fld,"-")
			if (eval[i] == "#") {
				print "   for (i = 0;v[i];i++)"
				print "       if ((v[i] < '0') || (v[i] > '9')) {"
				print "          parmerr."i" = 1;"
				print "          return 0;"
				print "       }"
			}
			if (fld[0] > 0)
				print "   if (strlen(v) < "fld[1]") {parmerr."i" = 1;return 0;}"
			print "   if (strlen(v) > "fld[2]") {parmerr."i" = 1;return 0;}"
			if (eval[i] == "#")
				eval_number(i)
			else
				eval_text(i)
		}
		else
			print "   return 0;"
		print "}"
	}

	print "int eval_parm(char *k,char *v) {"
	    print "   if (strcmp(k,\"key\") == 0) return 1;"
	for (i in el_range)
	    print "   if (strcmp(k,\"" i "\") == 0) return eval_"i"(v);"
	print "   return 0;"
	print "}"

	print "char shared_buffer[1024];"
	for (i in el_range) {
		if (eval[i] ~ /[#@]/) {
			print "char *as_text_"i"(void) {"
			print "   char *buf = shared_buffer;"
			if (eval[i] == "#")
				in_buf_number(i)
			else
				in_buf_text(i)
			print "   return shared_buffer;"
			print "}"
		}
	}

	print "void empty_checkboxes(unsigned int page) {"
	suffix=""
	for (i in checkboxes) {
		print "   if (page == "i") {"
		n = split(checkboxes[i],r,":")
		for (j = 2;j <= n;j++)
			print "      key." r[j] " = 0;"
		print "   }"
	}
	print "}"

	print "int present_page(unsigned int page) {"
	suffix=""
	for (i = 0;i <= page;i++) {
		print "   if (page == "i")"
		if (i in pagecond)
			print "      return " pagecond[i] ";"
		else
			print "      return 1;"
	}
	print "   return 0;"
	print "}"

	f="auto/keystruct.h"
	print "#define KEY_LEN sizeof(struct key_s)" >f
	for (i in el_range)
		if (eval[i] ~ /[#@]/)
			print "char *as_text_"i"(void);" >f
	print "struct key_s {" >f
	split("@:#:0-n",evals,":")
	for (k = 1;k <= 3;k++) {
		ev = evals[k]
		for (i in el_range)
			if (eval[i] == ev) {
				if (ev == "0-n")
					print "   unsigned int " i ":" el_bits[i] "; // " el_range[i] >f
				if (ev == "@") {
					split(elen[i],fld,"-")
					print "   char " i "[" fld[2] "]; // text" >f
				}
				if (ev == "#") {
					split(elen[i],fld,"-")
					n = fld[2]
					while (n >= 3) {
						print "   unsigned int " i "3_" int(n/3) ":10; // 0=0|1=1001..1010|3=1..1000 digits" >f
						n-=3
					}
					if ((n == 1) || (fld[2] == 3))
						print "   unsigned int " i "1:4; // 0=0|1=1..10 digits"  >f
					if (n == 2)
						print "   unsigned int " i "2:7; // 0=0|1=101..110|2=1..100 digits" >f
				}
			}
	}

	m=2
	for (bits=1;m < page;bits++)
		m *= 2

	print "   unsigned int page:" bits ";" >f
	print "} key;" >f

	print "struct parmerr_s {" >f
	for (i in el_range) {
		print "   int " i ":1;" >f
	}
	print "} parmerr;" >f

	for (i in flagcond)
		print "#define "i" " flagcond[i] >f
}
