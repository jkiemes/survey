all:	create

clean:
	find . -name "*~" -type f -exec rm "{}" \;

create:
	rm -fR testbed
	mkdir -m 711 testbed
	mkdir -m 711 testbed/cgi
	mkdir -m 711 testbed/log
	(cd src;make)
	cp src/auto/survey testbed/cgi
	(cd testbed/cgi;ln survey survey.prev)
	chmod 711 testbed/cgi/survey
	killall thttpd || true
	killall mini-httpd || true
	if [ -f /usr/sbin/thttpd ]; then thttpd -p 8888 -c /cgi/* -dd ./testbed; fi
	if [ -f /usr/sbin/mini-httpd ]; then mini-httpd -p 8888 -c cgi/* -dd ./testbed -u jochen;fi

do_publish:
	mount testbed
	sudo mkdir -p -m 711 testbed/cgi-bin
	sudo mkdir -p -m 711 testbed/log
	sudo chown 8000.8000 testbed/cgi-bin
	(cd src;make)
	sudo rm testbed/cgi-bin/survey
	sudo rm testbed/cgi-bin/survey.prev
	sudo cp src/auto/survey testbed/cgi-bin
	sudo ln testbed/cgi-bin/survey testbed/cgi-bin/survey.prev
	sudo chmod 711 testbed/cgi-bin/survey
	sudo chmod 711 testbed/cgi-bin/survey.prev
	sudo chown 8000.8000 testbed/cgi-bin/survey
	sudo chown 8000.8000 testbed/cgi-bin/survey.prev
	umount testbed
	./publish
